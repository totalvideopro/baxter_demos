#!/usr/bin/env python

import collections

from copy import deepcopy

import rospy

import tf
import cv2
import cv_bridge
import rospkg
import tf
import sys, argparse

from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion
)

from std_msgs.msg import Header

from sensor_msgs.msg import (
    Image,
    JointState,
)

import baxter_interface

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

from baxter_demos.srv import *

class PickPlace(object):
    def __init__(self, limb):
        self.side = limb
        self._limb = baxter_interface.Limb(limb)

        circle_io = baxter_interface.DigitalIO(limb + '_lower_button')
        dash_io = baxter_interface.DigitalIO(limb + '_upper_button')
        self.calibrating = False
        self.object_calib = -1
        self.objects = ['neutral', 'placemat', 'cup', 'plate', 'fork', 'spoon', 'knife', 'bowl', 'soda', 'wineglass']
        # self.objects = ['neutral', 'cup']
        self.object_pick_joint_angles = dict()
        self.object_pick_poses = dict()
        self.object_place_joint_angles = dict()
        self.object_place_poses = dict()

        self._gripper = baxter_interface.Gripper(limb)
        self._gripper.calibrate()
        self._gripper.set_holding_force(100.0)

        ik_srv = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"
        self._iksvc = rospy.ServiceProxy(ik_srv, SolvePositionIK)
        self._ikreq = SolvePositionIKRequest()

        circle_io.state_changed.connect(self.CirclePressed_)

    def CirclePressed_(self, value):
        if value and self.object_calib != -1:
            if self.pick:
                print "Calibrating Object Pick: " + self.objects[self.object_calib] + "..."
                self.object_pick_joint_angles[self.objects[self.object_calib]] = self._limb.joint_angles()
                self.object_pick_poses[self.objects[self.object_calib]] = self._limb.endpoint_pose()
                print "Calibrated"
                self.calibrating = False
            else:
                print "Calibrating Object Place: " + self.objects[self.object_calib] + "..."
                self.object_place_joint_angles[self.objects[self.object_calib]] = self._limb.joint_angles()
                self.object_place_poses[self.objects[self.object_calib]] = self._limb.endpoint_pose()
                print "Calibrated"
                self.calibrating = False

    def PickAndPlaceObject(self, req):
        print "Picking UP Object: " + req.object
        self._limb.set_joint_position_speed(0.8)
        self._limb.move_to_joint_positions(self.object_pick_joint_angles['neutral'])
        self._gripper.command_position(100.0)
        rospy.sleep(0.4)
        self._limb.move_to_joint_positions(self.object_pick_joint_angles[req.object])
        self._gripper.command_position(0.0)
        rospy.sleep(0.4)
        self._limb.move_to_joint_positions(self.object_pick_joint_angles['neutral'])

        print "Placing Down Object:" + req.object
        self._limb.move_to_joint_positions(self.object_place_joint_angles['neutral'])
        self._limb.move_to_joint_positions(self.object_place_joint_angles[req.object])
        self._gripper.command_position(100.0)
        rospy.sleep(0.4)
        self._limb.move_to_joint_positions(self.object_place_joint_angles['neutral'])
        self._gripper.command_position(0.0)

        return pick_and_placeResponse(True)
    def CalibrateObjects(self):
        '''
        Calibrate Objects: cup
        '''
        print "Calibrating Objects:"
        # Calibrate Cup
        for i, object in enumerate(self.objects):
            print "Move " + self.side + " limb to: " + object + " picking location and Press Circle"
            self.pick = True
            self.object_calib = i
            self.calibrating = True
            while self.calibrating and not rospy.is_shutdown():
                rospy.sleep(.1)
            self._gripper.command_position(0.0)
            print "Move " + self.side + " limb to: " + object + " Placing location and Press Circle"
            self.pick = False
            self.calibrating = True
            while self.calibrating and not rospy.is_shutdown():
                rospy.sleep(.1)
            self._gripper.command_position(100.0)

        self.object_calib = -1

    def _find_approach(self, pose, offset):
        ikreq = SolvePositionIKRequest()
        # Add 5 cm offset in Z direction
        try:
            pose['position'] = Point(x=pose['position'][0],
                                     y=pose['position'][1],
                                     z=pose['position'][2] + offset
                                     )
        except Exception:
            pose['position'] = Point(x=pose['position'].x,
                                     y=pose['position'].y,
                                     z=pose['position'].z + offset
                                     )
        approach_pose = Pose()
        approach_pose.position = pose['position']
        approach_pose.orientation = pose['orientation']

        hdr = Header(stamp=rospy.Time.now(), frame_id='base')
        pose_req = PoseStamped(header=hdr, pose=approach_pose)
        ikreq.pose_stamp.append(pose_req)
        resp = self._iksvc(ikreq)
        return dict(zip(resp.joints[0].name, resp.joints[0].position))

    def _find_jp(self, pose):
        ikreq = SolvePositionIKRequest()

        goal_pose = Pose()
        goal_pose.position = pose['position']
        goal_pose.orientation = pose['orientation']

        hdr = Header(stamp=rospy.Time.now(), frame_id='base')
        pose_req = PoseStamped(header=hdr, pose=goal_pose)
        ikreq.pose_stamp.append(pose_req)
        resp = self._iksvc(ikreq)
        return dict(zip(resp.joints[0].name, resp.joints[0].position))

    def ReadCalibration(self, filename):
        with open(filename, 'r') as f:
            for line in f:
                split = line.split('%')
                location = split[0]
                pick_or_place = location.split('_')[0]
                key = location.split('_')[1]
                position = split[1]
                if pick_or_place == 'pickAngles':
                    self.object_pick_joint_angles[key] = eval(position)
                elif pick_or_place == 'pickPose':
                    self.object_pick_poses[key] = eval(position)
                elif pick_or_place == 'placeAngles':
                    self.object_place_joint_angles[key] = eval(position)
                elif pick_or_place == 'placePose':
                    self.object_place_poses[key] = eval(position)

    def SaveCalibration(self, filename):
        f = open(filename, 'w')
        for key in self.object_pick_joint_angles:
            f.write('pickAngles_' + key + '%' + str(self.object_pick_joint_angles[key]) + '\n')
            f.write('pickPose_' + key + '%' + str(self.object_pick_poses[key]) + '\n')
        for key in self.object_place_joint_angles:
            f.write('placeAngles_' + key + '%' + str(self.object_place_joint_angles[key]) + '\n')
            f.write('placePose_' + key + '%' + str(self.object_place_poses[key]) + '\n')
        f.close()

    def PostParameters(self):
        for key in self.object_pick_poses:
            # Post param for Pick Position
            rospy.set_param('/ObjectPositions/' + key,
                [self.object_pick_poses[key]['position'].x,
                 self.object_pick_poses[key]['position'].y,
                 self.object_pick_poses[key]['position'].z])


def main():
    rospy.init_node("pick_and_place_service")

    parser = argparse.ArgumentParser(description='Process Pick and Place Command Line Arguments')
    parser.add_argument('--save', '-s', type=str)
    parser.add_argument('--read', '-r', type=str)
    args = parser.parse_args()

    rs = baxter_interface.RobotEnable()
    print("Enabling Robot")
    rs.enable()
    pp = PickPlace('right')

    # Calibrate Pickup Locations
    if args.read:
        print "Reading Position File: " + args.read
        pp.ReadCalibration(args.read)
    else:
        pp.CalibrateObjects()

    # Post Params
    pp.PostParameters();
    # Save Calibration to File
    if args.save:
        print "Saving Calibration File: " + args.save
        pp.SaveCalibration(args.save)

    # Advertise Service
    s = rospy.Service('pick_and_place_object', pick_and_place, pp.PickAndPlaceObject)
    print "READY to PICK and Place"
    rospy.spin()

if __name__ == '__main__':
    main()