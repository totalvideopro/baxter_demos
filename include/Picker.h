//
// Created by luke on 7/27/15.
//

#ifndef BAXTER_DEMOS_PICKER_H
#define BAXTER_DEMOS_PICKER_H
namespace pick_and_place {
class Picker {
public:
  Picker();

  virtual ~Picker();

private:
  virtual bool plan_to_location();

  virtual bool execute_to_location();
};

class CupPicker : public Picker {
public:
  CupPicker();

  ~CupPicker();

  bool PickupCup();
};
}
#endif //BAXTER_DEMOS_PICKER_H
