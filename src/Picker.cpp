//
// Created by luke on 7/27/15.
//

#include "Picker.h"
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <robotics_task_tree_eval/node.h>

#include <moveit_msgs/DisplayTrajectory.h>
namespace pick_and_place {
Picker::Picker() {
  ros::NodeHandle nh;
  moveit::planning_interface::MoveGroup group("right_arm");
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  ros::Publisher display_pub = nh.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);

  moveit_msgs::DisplayTrajectory display_trajectory;

  ROS_INFO("Reference frame: %s", group.getPlanningFrame().c_str());
  ROS_INFO("Reference frame: %s", group.getEndEffectorLink().c_str());

  geometry_msgs::Pose target_pose1;
  target_pose1.orientation.w = 1.0;
  target_pose1.position.x = 0.28;
  target_pose1.position.y = -0.7;
  target_pose1.position.z = 0.5;
  group.setPoseTarget(target_pose1);
  moveit::planning_interface::MoveGroup::Plan my_plan;
  bool success = group.plan(my_plan);

  ROS_INFO("Visualizing Plan 1 (pose goal) %s", success?"":"Failes");
  ROS_INFO("Moving ARMS Now!");
  sleep(1.0);
  group.move();

  sleep(0.5);

  target_pose1.orientation.w = 1.0;
  target_pose1.position.x = 0.38;
  target_pose1.position.y = -0.7;
  target_pose1.position.z = 0.5;
  group.setPoseTarget(target_pose1);
  success = group.plan(my_plan);
  group.move();

  // Initialize Picker

}

Picker::~Picker() {

}

bool Picker::plan_to_location() {
  return false;
}

bool Picker::execute_to_location() {
  return false;
}

CupPicker::CupPicker() {

}

CupPicker::~CupPicker() {

}

bool CupPicker::PickupCup() {
  return false;
}
}
